pragma solidity ^0.5.17;

import "./Roles.sol";


/**
 * @title Exchange interface
 */
interface ExchangeInterface {
    function setMainStatus(bool status) external;
    function setTokensContract(address contractAddress) external;

    function cancelOrder(uint orderId) external returns(bool);
    function fillOrder(uint orderId, uint buyTokenId) external returns(bool);

    event SetMainStatus(bool mainStatus);
    event SetTokensContract(address contractAddress);

    event CreateOrder(uint orderId, address owner, uint sellTokenId, string buyTokenName);
    event CancelOrder(uint orderId);
    event FillOrder(address owner, uint buyTokenId, uint orderId);
}


/**
 * @title ERC721 token interface
 */
interface ERC721Interface {
    function getApproved(uint256 tokenId) external view returns (address);
    function tokenName(uint256 tokenId) external view returns (string memory);
    function transferFrom(address from, address to, uint256 tokenId) external;
}


/**
 * @title Exchange
 * @dev The Exchange contract is a smart contract.
 * It controll tokens transferring.
 */
contract Exchange is ExchangeInterface, Roles {
    bool public mainStatus;

    ERC721Interface public tokensContract;

    struct Order {
        uint orderId;
        address payable owner;
        uint sellTokenId;
        string buyTokenName;
        bool status;
    }

    mapping (uint => Order) public orders;
    uint public ordersCount;

    constructor(address contractAddress) public {
        mainStatus = true;
        ordersCount = 0;
        tokensContract = ERC721Interface(contractAddress);
    }

    modifier isActive {
        require(mainStatus, "Platform is stopped.");
        _;
    }

    modifier checkOrder(uint sellTokenId) {
        require(tokensContract.getApproved(sellTokenId) == address(this), "Can't get allowance.");
        _;
    }

    modifier checkCancelOrder(uint orderId) {
        require(orders[orderId].status, "Wrong order status.");
        require(orders[orderId].owner == msg.sender, "Not an owner.");
        _;
    }

    modifier checkFillOrder(uint orderId, uint buyTokenId) {
        require(orders[orderId].status, "Wrong order status.");
        require(keccak256(abi.encodePacked((tokensContract.tokenName(buyTokenId)))) == keccak256(abi.encodePacked((orders[orderId].buyTokenName))), "Wrond token.");
        _;
    }

    /**
     * @dev Set main status. Stop button.
     * @param status bool The main status
     */
    function setMainStatus(bool status) external onlyOwner {
        mainStatus = status;
        emit SetMainStatus(status);
    }

    /**
     * @dev Set Tokens contract.
     * @param contractAddress address The tokens contract address.
     */
    function setTokensContract(address contractAddress) external onlyOwner {
        tokensContract = ERC721Interface(contractAddress);
        emit SetTokensContract(contractAddress);
    }

    /**
     * @dev Create oreder.
     * @param sellTokenId uint The token ID for selling
     * @param buyTokenName string The token Name for buying
     */
    function createOrder(uint sellTokenId, string memory buyTokenName) public isActive checkOrder(sellTokenId) returns(uint id) {
        tokensContract.transferFrom(msg.sender, address(this), sellTokenId);

        Order memory order;
        id = ordersCount;

        order.orderId      = id;
        order.owner        = msg.sender;
        order.sellTokenId  = sellTokenId;
        order.buyTokenName = buyTokenName;
        order.status       = true;

        orders[id] = order;
        ordersCount++;

        emit CreateOrder(id, msg.sender, sellTokenId, buyTokenName);
        return id;
    }

    /**
     * @dev Cancel ERC721 oreder.
     * @param orderId uint The order id
     */
    function cancelOrder(uint orderId) external isActive checkCancelOrder(orderId) returns(bool) {
        orders[orderId].status = false;
        tokensContract.transferFrom(address(this), orders[orderId].owner, orders[orderId].sellTokenId);
        emit CancelOrder(orderId);
        return true;
    }

    /**
     * @dev Fill ERC721 oreder.
     * @param orderId uint The order id
     */
    function fillOrder(uint orderId, uint buyTokenId) external isActive checkFillOrder(orderId, buyTokenId) returns(bool) {
        orders[orderId].status = false;
        tokensContract.transferFrom(msg.sender, orders[orderId].owner, buyTokenId);
        tokensContract.transferFrom(address(this), msg.sender, orders[orderId].sellTokenId);

        emit FillOrder(msg.sender, buyTokenId, orderId);
        return true;
    }
}
