pragma solidity ^0.5.17;

import "./Roles.sol";
import "./ERC-721/SafeMath.sol";

/**
 * @title Store interface
 */
interface StoreInterface {
    function setItemStatus(uint256 itemId, bool status) external;
    function setItemCount(uint256 itemId, uint count) external;
    function setItemPrice(uint256 itemId, uint256 price) external;
    function buyItem(uint256 itemId) external payable;
    function setPool(address payable wallet) external;
    function setTokensContract(address contractAddress) external;
    function setMainStatus(bool status) external;

    event AddItem(uint id, uint256 price, uint count, string tokenName);
    event SetStatus(uint256 itemId, bool status);
    event SetCount(uint256 itemId, uint count);
    event SetPrice(uint256 itemId, uint256 price);
    event BuyItem(uint256 itemId, uint256 tokenId, address wallet);
    event SetPool(address wallet);
    event SetTokensContract(address contractAddress);
    event SetMainStatus(bool mainStatus);
}

/**
 * @title ERC721 token interface
 */
interface ERC721Interface {
    function mintNext(address to, string calldata tokenName) external returns (uint index);
}

/**
 * @title Store
 * @dev The Store contract is a smart contract.
 * It controll tokens minting.
 */
contract Store is StoreInterface, Roles {
    using SafeMath for uint;

    address payable public pool;
    bool public mainStatus;

    ERC721Interface public tokensContract;

    struct Item {
        uint256 price;
        uint count;
        string tokenName;
        bool status;
    }

    mapping (uint256 => Item) public storeItems;
    uint storeItemsCount;

    constructor(address contractAddress) public {
        mainStatus = true;
        pool = msg.sender;
        tokensContract = ERC721Interface(contractAddress);
    }

    modifier isActive {
        require(mainStatus, "Platform is stopped.");
        _;
    }

    modifier chckItem(uint256 itemId) {
        require(storeItems[itemId].status, "Item is inactive.");
        require(storeItems[itemId].count > 0, "Items ran out.");
        require(storeItems[itemId].price == msg.value, "Not enought funds.");
        _;
    }

    /**
     * @dev Add store item.
     * @param price uint256 The new item price.
     * @param count uint The new item count.
     * @param tokenName string The new item title.
     */
    function addItem(uint256 price, uint count, string memory tokenName) public isActive onlyOwner returns(uint id) {
        Item memory item;
        id = storeItemsCount;

        item.price     = price;
        item.count     = count;
        item.tokenName = tokenName;
        item.status    = true;

        storeItems[id] = item;
        storeItemsCount++;

        emit AddItem(id, price, count, tokenName);
        return id;
    }

    /**
     * @dev Set item status.
     * @param itemId uint256 The item Id.
     * @param status bool The new status value.
     */
    function setItemStatus(uint256 itemId, bool status) external onlyOwner {
        storeItems[itemId].status = status;
        emit SetStatus(itemId, status);
    }

    /**
     * @dev Set item count.
     * @param itemId uint256 The item Id.
     * @param count uint The new count value.
     */
    function setItemCount(uint256 itemId, uint count) external onlyOwner {
        storeItems[itemId].count = count;
        emit SetCount(itemId, count);
    }

    /**
     * @dev Set item price.
     * @param itemId uint256 The item Id.
     * @param price uint256 The new count value.
     */
    function setItemPrice(uint256 itemId, uint256 price) external onlyOwner {
        storeItems[itemId].price = price;
        emit SetPrice(itemId, price);
    }

    /**
     * @dev Buy item.
     * @param itemId uint256 The item Id.
     */
    function buyItem(uint256 itemId) external payable chckItem(itemId) {
        pool.transfer(msg.value);
        storeItems[itemId].count = storeItems[itemId].count.sub(1);

        uint tokenId = tokensContract.mintNext(msg.sender, storeItems[itemId].tokenName);
        emit BuyItem(itemId, tokenId, msg.sender);
    }

    /**
     * @dev Set pool wallet.
     * @param wallet address The new pool wallet address.
     */
    function setPool(address payable wallet) external onlyOwner {
        pool = wallet;
        emit SetPool(wallet);
    }

    /**
     * @dev Set Tokens contract.
     * @param contractAddress address The tokens contract address.
     */
    function setTokensContract(address contractAddress) external onlyOwner {
        tokensContract = ERC721Interface(contractAddress);
        emit SetTokensContract(contractAddress);
    }

    /**
     * @dev Set main status. Stop button.
     * @param status bool The main status.
     */
    function setMainStatus(bool status) external onlyOwner {
        mainStatus = status;
        emit SetMainStatus(status);
    }
}
