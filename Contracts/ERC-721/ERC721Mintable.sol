pragma solidity ^0.5.17;

import "./ERC721.sol";
import "./MinterRole.sol";

/**
 * @title ERC721Mintable
 * @dev ERC721 minting logic.
 */
contract ERC721Mintable is ERC721, MinterRole {
    using SafeMath for uint;

    uint counter = 0;

    /**
     * @dev Function to mint tokens.
     * @param to The address that will receive the minted token.
     * @param tokenId The token id to mint.
     * @return A boolean that indicates if the operation was successful.
     */
    function mint(address to, uint256 tokenId, string memory tokenName) public onlyMinter returns (bool) {
        _mint(to, tokenId, tokenName);
        counter = counter.add(1);
        return true;
    }

    /**
     * @dev Function to mint tokens. Set `tokenId` as last plus 1;
     * @param to The address that will receive the minted token.
     * @return A boolean that indicates if the operation was successful.
     */
    function mintNext(address to, string memory tokenName) public onlyMinter returns (uint index) {
        _mint(to, counter, tokenName);
        index = counter;
        counter = counter.add(1);
    }

    /**
     * @dev Function to safely mint tokens.
     * @param to The address that will receive the minted token.
     * @param tokenId The token id to mint.
     * @return A boolean that indicates if the operation was successful.
     */
    function safeMint(address to, uint256 tokenId, string memory tokenName) public onlyMinter returns (bool) {
        _safeMint(to, tokenId, tokenName);
        counter = counter.add(1);
        return true;
    }

    /**
     * @dev Function to safely mint tokens.
     * @param to The address that will receive the minted token.
     * @param tokenId The token id to mint.
     * @param _data bytes data to send along with a safe transfer check.
     * @return A boolean that indicates if the operation was successful.
     */
    function safeMint(address to, uint256 tokenId, string memory tokenName, bytes memory _data) public onlyMinter returns (bool) {
        _safeMint(to, tokenId, tokenName, _data);
        counter = counter.add(1);
        return true;
    }
}
