pragma solidity ^0.5.17;


/**
 * @title Roles interface
 */
interface RolesInterface {
    function transferOwnership(address newOwner) external returns (bool);
    function acceptOwnership() external returns (bool);

    event OwnershipTransferred(address indexed from, address indexed to);
}


/**
 * @title Roles
 * @dev The Roles contract has an owner and moderator address, and provides
 * basic authorization control functions, this simplifies the implementation
 * of "user permissions".
 */
contract Roles is RolesInterface {
    address public owner;
    address public newOwner;

    constructor() public {
        owner = msg.sender;
    }

    modifier onlyOwner {
        require(msg.sender == owner, "msg.sender is not an owner.");
        _;
    }

    /**
     * @dev Create transfer ownership request.
     * @param _newOwner address The address of new owner
     * @return True if the operation was successful
     */
    function transferOwnership(address _newOwner) external onlyOwner returns (bool) {
        newOwner = _newOwner;
        return true;
    }

    /**
     * @dev Accept transfer ownership request.
     * @return True if the operation was successful
     */
    function acceptOwnership() external returns (bool) {
        require(msg.sender == newOwner, "msg.sender is not a newOwner.");
        emit OwnershipTransferred(owner, newOwner);
        owner = newOwner;
        newOwner = address(0);
        return true;
    }
}
