pragma solidity ^0.5.17;

/**
 * @title Files interface
 */
interface FilesInterface {
    event RequestAccess(address wallet, string key);
}

/**
 * @title Files
 * @dev The Files contract is a smart contract.
 * It controll files access.
 */
contract Files is FilesInterface {
    /**
     * @dev Add access request.
     * @param key string The access key.
     */
    function requestAccess(string memory key) public returns(bool) {
        emit RequestAccess(msg.sender, key);
        return true;
    }
}
