# Smart Contracts
v1.0.0 May 31, 2020

Smart contracts for Smart Books.

### Deployment instruction

1. Deploy **ERC721Full** contract.

```
name = Smart Books
symbol = SB
```

2. Deploy **Store** contract:

```
contractAddress = < ERC721Full address >
```

3. Connect **Store** to **ERC721Full** contract.

```
ERC721Full.addMinter(< Store address >);
```

4. Add item to **Store**:

```
Store.addItem(1000000000, 5, 'Book #1');
```

5. Buy item

```
Store.buyItem(< item Id >);
```

6. Deploy **Exchange** contract:

```
contractAddress = < ERC721Full address >
```

7. Create an order:

```
ERC721Full.approve(< Exchange address >, < token ID >);
Exchange.createOrder(< token ID >, 'Book #2');
```

8. Fill an order:

```
ERC721Full.approve(< Exchange address >, < token ID >);
Exchange.fillOrder(< order ID >, < token ID >);
```

9. Enjoy.

Powered by DenyStark 2020.
